

Markdown
# AWS Three-Tier Architecture Automation with Terraform and GitLab CI/CD

![Diagram of AWS Three-Tier Architecture](https://gitlab.com/kyawzawaung321/aws-three-tier-architecture-with-terraform/-/raw/main/3_tier_aws.png) 


# Project Overview

This project showcases the automation of a three-tier Java application deployment on Amazon Web Services (AWS). The application retrieves user information from a database and leverages the following technologies:

* **Three-Tier Architecture:** The application follows a classic three-tier model:
    * **Presentation Tier:** Handles the user interface and web requests.
    * **Application Tier:** Houses the Java application logic and interacts with the database. 
    * **Database Tier:** Provides persistent storage for user information.

* **Infrastructure as Code (IaC):** Terraform config files define AWS resources for each tier (e.g., EC2 instances, load balancers, RDS database).

* **Containerization:** The Java application is packaged as a Docker image, ensuring portability and consistency.

* **Amazon ECR:** Securely stores and manages the application's Docker image.

* **Shell Scripting:** Custom scripts automate:
    * ECR repository creation
    * Docker image building
    * Image pushing to ECR

* **GitLab CI/CD:** The CI/CD pipeline orchestrates the process, triggering on code changes. It executes Terraform commands, shell scripts, and handles application deployment logic.

**Benefits**

* **Consistency:**  IaC and containerization ensure a predictable and repeatable application environment.
* **Efficiency:**  Automation significantly speeds up development and deployment.
* **Scalability:** The three-tier architecture on AWS provides a foundation for scaling the application horizontally or vertically.
* **Collaboration:**  Git-based versioning allows for effective teamwork.


